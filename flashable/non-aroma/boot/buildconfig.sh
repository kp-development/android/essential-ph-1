#!/sbin/sh

#Build config file
CONFIGFILE="/tmp/init.elementalx.rc"
BACKUP="/sdcard/.elementalx.backup"

#reinstall options
echo -e "##### Reinstall Options #####" > $BACKUP
echo -e "# These settings are only applied if you run the express installer" >> $BACKUP

echo "CPU0=1900MHz" >> $BACKUP;

echo "CPU4=2457MHz" >> $BACKUP;

