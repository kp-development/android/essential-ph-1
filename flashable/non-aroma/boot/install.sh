#!/sbin/sh

/tmp/busybox fstrim /data

SLOT=$(for i in `cat /proc/cmdline`; do echo $i | grep slot | dd bs=1 skip=24 2>/dev/null; done)
BOOTPATH="/dev/block/bootdevice/by-name/boot$SLOT"

/tmp/busybox dd if=$BOOTPATH of=/tmp/boot.img
/tmp/unpackbootimg -i /tmp/boot.img -o /tmp/

cpu_max_c1="cpu_max_c1=1900800"
cpu_max_c2="cpu_max_c2=2457600"

if  grep cpu_max_c1 /tmp/boot.img-cmdline; then
	sed -i "s/cpu_max_c1.*/$cpu_max_c1 $cpu_max_c2 /" /tmp/boot.img-cmdline
else
	echo -n $cpu_max_c1 $cpu_max_c2 >> /tmp/boot.img-cmdline
fi

mkdir /tmp/ramdisk
cp /tmp/boot.img-ramdisk.gz /tmp/ramdisk/
cd /tmp/ramdisk/
gunzip -c /tmp/ramdisk/boot.img-ramdisk.gz | /tmp/busybox cpio -i
rm /tmp/ramdisk/boot.img-ramdisk.gz

if [ -d /tmp/ramdisk/.subackup -o -d /tmp/ramdisk/.backup ]; then

	#Override skip_initramfs
	if ! grep -q skip_override /tmp/boot.img-cmdline; then
		echo -n " skip_override" >> /tmp/boot.img-cmdline
	fi

else
	#remove skip_override
	if grep -q skip_override /tmp/boot.img-cmdline; then
		sed -i "s/skip_override//" /tmp/boot.img-cmdline
	fi

fi

echo \#!/sbin/sh > /tmp/createnewboot.sh
echo /tmp/mkbootimg --kernel /tmp/elex.Image --ramdisk /tmp/boot.img-ramdisk.gz --cmdline \"$(cat /tmp/boot.img-cmdline)\" --base 00000000 --kernel_offset 00008000 --pagesize 4096 --ramdisk_offset 01000000 --tags_offset 00000100 --second_offset 0x00f00000 --os_version \"$(cat /tmp/boot.img-osversion)\" --os_patch_level \"$(cat /tmp/boot.img-oslevel)\" --hash sha1 --output /tmp/newboot.img >> /tmp/createnewboot.sh
chmod 777 /tmp/createnewboot.sh
/tmp/createnewboot.sh

/tmp/busybox dd if=/tmp/newboot.img of=$BOOTPATH


