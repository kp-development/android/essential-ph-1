#!/sbin/sh

#Build config file
CONFIGFILE="/tmp/init.elementalx.rc"
BACKUP="/sdcard/.elementalx.backup"

#reinstall options
echo -e "##### Reinstall Options #####" > $BACKUP
echo -e "# These settings are only applied if you run the express installer" >> $BACKUP

#Maximum CPU freqs
CPU0=$(cat /tmp/aroma/cpu0.prop | cut -d '=' -f2)

echo -e "\n\n##### Max Little Cluster Frequency #####" >> $BACKUP
echo -e "# 1=2035MHz\n" >> $BACKUP
echo -e "# 2=1900MHz 3=1824MHz" >> $BACKUP
echo -e "# 4=1747MHz 5=1555MHz" >> $BACKUP
echo -e "# 6=1401MHz" >> $BACKUP

echo "CPU0=$CPU0" >> $BACKUP;

CPU4=$(cat /tmp/aroma/cpu2.prop | cut -d '=' -f2)

echo -e "\n\n##### Max Big Cluster Frequency #####" >> $BACKUP
echo -e "# 1=2592MHz\n" >> $BACKUP
echo -e "# 2=2457MHz 3=2361MHz" >> $BACKUP
echo -e "# 4=2265MHz 5=2035MHz" >> $BACKUP
echo -e "# 6=1804MHz 7=1651MHz" >> $BACKUP

echo "CPU4=$CPU4" >> $BACKUP;

